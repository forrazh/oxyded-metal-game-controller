# Oxyded Metal Game Controller

## Important Information

Project done in regard to the PJI ("Projet Individuel", Individual Project) of my Master'
s degree.  
Everything in this repository has been done through educational purposes.

Contact :
- [Hugo Forraz](mailto://hugo@forraz.com)

## How to launch 

### Needed tools

To start this project, you will need to install [`cargo`](https://doc.rust-lang.org/cargo/getting-started/installation.html).

You will also need to install the following dependencies :
- Through cargo :
  + `cargo install elf2uf2-rs`
- Through rustup :
  + `rustup target add thumbv6m-none-eabi`
- Through your package manager :
  + `pico-sdk`
  + `picotool`

If you want to flash using another Raspberry (tried it out with a Pi4), I did the following :
- Install the rust toolchain
- Follow some chapters of the [Raspberry PicoGetting Started sheet](./https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf)
  + Get all the base tools: Chapter 1. Quick Pico Setup
  + Flash through USB: Chapter 3. Blinking an LED in C 
    * Note that you will need `elf2uf2-rs` to build an uf2
  + Flash through UART/SW: Chapter 5. Flash Programming with SWD

If you want to program on your computer and debug from it, you might want to check out how you can remotely flash with OpenOCD. I managed to build a CLion Run Configuration to do so, the same should be feasible on most IDEs. I used the following tools :
- `cargo` to compile (`cargo build` or `cargo build -r` for a release build)
- `ssh -t -L 3333:127.0.0.1:3333 pi@<pi_ip> openocd -f interface/raspberrypi-swd.cfg -f target/rp2040.cfg`
- On Pi (only tried it with UART/SWD) :
  * `openocd`
  * `gdb-multiarch` to debug 
      + `target remote :3333`


> You can get the same configuration for CLion by looking at the [`runConfigurations`](./runConfigurations) folder. Make sure to change the IP address !

### Build and flash

#### Preamble

Before flashing, you will need to enter in BOOTSEL mode on the Raspberry, once you are in
BOOTSEL, you can see if your Raspberry is available through `picotool info` (this will 
also make it available to be flashed).

#### The case

If you have a 3d printer, you can build a case for the controller using the models available 
in the [`3d_models`](./3d_models/).

#### Wiring the peripherals

Here is the wiring, you can also take at the pictures to see what it looks like on my proof
of concept. The pictures are available in the [controller_pictures](./controller_pictures).


Joystick :

| JoyPin | RaspPin number | Pin Name | Wire's color |
|--------|----------------|----------|--------------|
| GND    | 38             | GND      | Yellow       |
| +5V    | 36             | 3V3 OUT  | Green        |
| VRx    | 31             | ADC0     | Blue         |
| VRy    | 32             | ADC1     | Purple       |
| SW     | 21             | GP16     | White        |

Buttons :

| Button direction | RaspPin Number | Pin name | Wire's color |
|------------------|----------------|----------|--------------|
| GND              | 13             | GND      | Black        |
| Power in         | 24             | GP18     | Red          |
| UP               | 9              | GP6      | Brown        |
| DOWN             | 10             | GP7      | Yellow       |
| RIGHT            | 11             | GP8      | Blue         |
| LEFT             | 12             | GP9      | White        |


If you want to easily flash another program or reset the controller, you can add a button and 2 wires.
You need one of the wires to be connected to a ground and the other one to `RUN` (Pin 30)... And both 
wires should be connected to the push button.

#### The actual thing

You can only compile with the following command :

```sh
cargo build # add -r if you want a release build
```

You can only flash the built executable through this command :

```sh
elf2uf2-rs -d <location_of_exec>
```

You can also do both at once with :

```sh
cargo run # add -r for a release build
```

## Work done

Take a look at Gitlab's issues to get an idea of what have been done and what still have 
to be done.

### Communicating through USB 

While communicating through USB, you'll have a few things that will appear. As I tested this on Linux, I'll just provide the Linux data.
- `lsusb` will show you the device as `0000:cafe hfrz Oxyded Game Controller`.
- A `ttyACMx` file will appear in `/dev/` (where x is a number), this is the logger file.
- An `eventx` and a `jsx` file will appear in `/dev/input/` (where x is a number), those are the HID files. 
  + Please note that for now, only the `eventx` file is used. 

```sh
λ lsusb
[...]
Bus 001 Device 004: ID 0000:cafe hfrz Oxyded Game Controller
[...]

λ ls /dev | grep ttyACM
crw-rw----   166,0 root  1 juin  10:01  ttyACM0

λ ls /dev/input
drwxr-xr-x      - root  1 juin  10:01  by-id
[...]
crw-rw----@ 13,79 root  1 juin  10:01  event15
crw-rw-r--@  13,0 root  1 juin  10:01  js0
[...]

λ ls /dev/input/by-id
lrwxrwxrwx@ 10 root  1 juin  10:01  usb-hfrz_Oxyded_Game_Controller_000000000001-event-joystick -> ../event15
lrwxrwxrwx@  6 root  1 juin  10:01  usb-hfrz_Oxyded_Game_Controller_000000000001-joystick -> ../js0 # currently not used
[...]
```

### Communicating through Bluetooth

[WIP]

This part is a bit more tricky as Bluetooth support on Pico is quite recent (February 2023) and hasn't been really handled on Embassy (specifics for the CYW43 have been merged on May 30th on the main repo).
Here, I'll try to use the C ABI and FFI provided by the Pico SDK and use the provided Bluetooth stack to try to make it work.

I took inspiration from the followings :
- https://github.com/bluekitchen/btstack/blob/master/example/hid_host_demo.c
- https://docs.rust-embedded.org/book/interoperability/c-with-rust.html
- https://apollolabsblog.hashnode.dev/rust-ffi-and-bindgen-integrating-embedded-c-code-in-rust#heading-step-3-generate-a-static-library-from-the-stm32-hal-c-project-output
- https://lib.rs/crates/bindgen

Here, the biggest issue is trying to compile the C Library to make it compliant with the Rust compilation. 
I currently have the following error : 

```
error: failed to run custom build command for `oxided_controller v0.0.1 (/home/dra/work-projects/pji-test)`
Caused by:
  process didn't exit successfully: `/home/dra/work-projects/pji-test/target/debug/build/oxided_controller-19a85de84b4a0f5f/build-script-build` (exit status: 101)
  --- stdout
  cargo:rustc-link-search=/home/dra/work-projects/pji-test/target/thumbv6m-none-eabi/debug/build/oxided_controller-e0b9582b48a1b0ea/out
  cargo:rustc-link-lib=static=btstack-posix-h4
  cargo:rerun-if-changed=memory.x
  cargo:rustc-link-arg-bins=--nmagic
  cargo:rustc-link-arg-bins=-Tlink.x
  cargo:rustc-link-arg-bins=-Tlink-rp.x
  cargo:rustc-link-arg-bins=-Tdefmt.x
  cargo:rerun-if-changed=wrapper.h
  cargo:rerun-if-env-changed=TARGET
  cargo:rerun-if-env-changed=BINDGEN_EXTRA_CLANG_ARGS_thumbv6m-none-eabi
  cargo:rerun-if-env-changed=BINDGEN_EXTRA_CLANG_ARGS_thumbv6m_none_eabi
  cargo:rerun-if-env-changed=BINDGEN_EXTRA_CLANG_ARGS
  --- stderr
  ./libs/btstack/btstack_util.h:52:10: fatal error: 'string.h' file not found
```
