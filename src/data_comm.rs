use defmt::Format;

/// This struct represents the retrieved through the ADC.
/// The values should be written on 12 bits.
#[derive(Copy, Clone, Debug, Format)]
pub struct JoystickInformation {
    pub m_x: u16,
    pub m_y: u16,
}

/// The different buttons used by the controller.
#[derive(Copy, Clone, Debug, Format)]
pub enum Button {
    Left, Right, Up, Down, Joy
}

/// The different states a button can be in.
#[derive(Copy, Clone, Debug, Format)]
pub enum ButtonState {
    Pressed, Released
}

/// This struct represents the information about a button.
/// With this, we can easily apply a binary XOR to get the new state to send.
#[derive(Copy, Clone, Debug, Format)]
pub struct ButtonInformation {
    pub m_new_button_state: ButtonState,
    pub m_button: Button,
}

/// Messages sent through the channel so the input and output core can share their state.
#[derive(Copy, Clone, Debug, Format)]
pub enum Message {
    ButtonChangedState(ButtonInformation),
    JoystickDirection(JoystickInformation),
    Sample
}

impl Message {
    pub fn get_button_data(p_button: Button, p_button_state: ButtonState) -> Message {
        Message::ButtonChangedState(ButtonInformation {
            m_new_button_state: p_button_state,
            m_button: p_button,
        })
    }

    pub fn get_joystick_data(p_x: u16, p_y: u16) -> Message {
        Message::JoystickDirection(JoystickInformation {
            m_x: p_x,
            m_y: p_y,
        })
    }
}