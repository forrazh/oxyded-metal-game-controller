use embassy_rp::peripherals::USB;
use embassy_rp::usb::Driver;
use embassy_usb::class::hid::HidWriter;
use embassy_usb_driver::EndpointError;
use usbd_hid::descriptor::AsInputReport;
use crate::CHANNEL;
use crate::data_comm::Message;
use crate::joystick::{ButtonHolder, ControllerReport, DEFAULT_JOYSTICK_VALUE};

/// Wrapper for the error
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[cfg_attr(feature = "defmt", derive(defmt::Format))]
pub enum HIDWriteError {
    USBError(EndpointError),
    BluetoothError,
}

/// Wrapper made to abstract the Bluetooth and USB communication.
pub enum HidCommunicatorType {
    USB(HidWriter<'static, Driver<'static, USB>, 5>),
    Bluetooth(HidBluetoothWrapper),
}

impl HidCommunicatorType {
    /// Delegate the work to the serializing method
    async fn write_serialize<IR: AsInputReport>(&mut self, r: &IR) -> Result<(), HIDWriteError> {
        match self {
            HidCommunicatorType::USB(hid) => hid.write_serialize(r).await.map_err(HIDWriteError::USBError),
            HidCommunicatorType::Bluetooth(hid) => hid.write_serialize(r).await.map_err(|_| HIDWriteError::BluetoothError),
        }
    }

}

/// Wrapper for the bluetooth structure
pub struct HidBluetoothWrapper {

}

impl HidBluetoothWrapper {
    async fn write_serialize<IR: AsInputReport>(&mut self, _r: &IR) -> Result<(), ()> {
        todo!()
    }
}


/// Task that will receive the events to communicate
#[embassy_executor::task]
pub async fn consuming_task(mut hid: HidCommunicatorType) {
    let mut l_buttons_holder = ButtonHolder::default();
    loop {
        let data = CHANNEL.recv().await;
        log::info!("Received {:?}", data);
        // Transform the received event
        let (l_x, l_y) = match data {
            Message::ButtonChangedState(button_information) => {
                l_buttons_holder.switch_button_value(button_information.m_button);
                (DEFAULT_JOYSTICK_VALUE, DEFAULT_JOYSTICK_VALUE)
            },
            Message::JoystickDirection(joystick_information) => {
                (joystick_information.m_x, joystick_information.m_y)
            },
            _ => (0, 0),
        };

        let report = ControllerReport::new(l_buttons_holder.get_buttons(), l_x, l_y);

        // and send it through the HID
        if let Err(e) = hid.write_serialize(&report).await {
            log::error!("{:?}", e);
        }
        log::info!("{{\n\r\tRetrieved {:?};\n\r\tNew data: {{x{};y{};{}}}\n\r}}", data, {report.buttons}, {report.x}, {report.y});
    }
}