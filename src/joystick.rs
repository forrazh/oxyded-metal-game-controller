extern crate usbd_hid_macros;
extern crate serde;

use crate::data_comm::Button;
use usbd_hid::descriptor::AsInputReport;
use serde::ser::{Serialize, SerializeTuple, Serializer};
use usbd_hid::descriptor::SerializedDescriptor;
pub mod generator_prelude {
    pub use usbd_hid::descriptor::{AsInputReport, SerializedDescriptor};
    pub use serde::ser::{Serialize, SerializeTuple, Serializer};
    pub use usbd_hid_macros::gen_hid_descriptor;
}
pub use usbd_hid_macros::gen_hid_descriptor;


pub const GREEN_BUTTON:    u8 = 0b00000001;
pub const RED_BUTTON:      u8 = 0b00000010;
pub const BLUE_BUTTON:     u8 = 0b00000100;
pub const YELLOW_BUTTON:   u8 = 0b00001000;
pub const JOYSTICK_BUTTON: u8 = 0b00010000;
pub const DEFAULT_JOYSTICK_VALUE: u16 = 0b100000000000;

/// Description of the controller for the HID.
/// You get a better description of how this works through these :
/// - https://docs.rs/usbd-hid/0.6.1/usbd_hid/ (lib)
/// - https://usb.org/sites/default/files/hut1_2.pdf (spec)
#[gen_hid_descriptor(
    (collection = APPLICATION, usage_page = GENERIC_DESKTOP, usage = JOYSTICK) = {
        (usage_page = BUTTON, usage_min = 0x01, usage_max = 0x05) = {
            #[packed_bits 8] #[item_settings data,variable,absolute] buttons=input;
        };
        (collection = PHYSICAL, usage = POINTER) = {
            (usage = X,) = {
                #[item_settings data,variable,relative] x=input;
            };
            (usage = Y,) = {
                #[item_settings data,variable,relative] y=input;
            };
        };
    }
)]
pub struct ControllerReport {
    pub buttons: u8,
    pub x: u16,
    pub y: u16,
}

impl ControllerReport {
    pub fn new(buttons: u8, x:u16, y:u16) -> ControllerReport {

        ControllerReport {
            buttons,
            x,
            y,
        }
    }
}

/// Struct that will hold the button's data to share it more consistently
#[derive(Default, Debug)]
pub struct ButtonHolder {
    buttons: u8
}

impl ButtonHolder {
    pub fn get_buttons(&self) -> u8 {
        self.buttons
    }
    pub fn new(p_value:u8) -> ButtonHolder {
        ButtonHolder {
            buttons: p_value,
        }
    }

    pub fn switch_button_value(&mut self, p_button: Button) {
        let button_value = match p_button {
            Button::Left => GREEN_BUTTON,
            Button::Up => RED_BUTTON,
            Button::Right => BLUE_BUTTON,
            Button::Down => YELLOW_BUTTON,
            Button::Joy => JOYSTICK_BUTTON,
        };

        self.buttons ^= button_value;
    }
}



