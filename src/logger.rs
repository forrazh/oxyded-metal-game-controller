use embassy_rp::peripherals::USB;
use embassy_rp::usb::Driver;
use embassy_sync::pipe::Pipe;
use embassy_usb::class::cdc_acm::CdcAcmClass;

/// Task used to log the data through USB.
/// On Linux, this will log any data to the `/dev/ttyACMx` serial port.
/// Most of what have been done comes from the base implementation reworked to be a bit more compliant with our use case.
#[embassy_executor::task]
pub async fn logger_task(mut acm_logger: CdcAcmClass<'static, Driver<'static, USB>>, logger: &'static MyLogger, log_level: log::LevelFilter) {
    const MAX_PACKET_SIZE: u8 = 64;

    unsafe {
        let _ = ::log::set_logger_racy(logger).map(|()| log::set_max_level(log_level));
    }
    let mut rx: [u8; MAX_PACKET_SIZE as usize] = [0; MAX_PACKET_SIZE as usize];
    acm_logger.wait_connection().await;
    loop {
        let len = logger.buffer.read(&mut rx[..]).await;
        let _ = acm_logger.write_packet(&rx[..len]).await;
    }
}

type CS = embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;

/// Declare this as a `singleton` to use the logger
pub struct MyLogger {
    buffer: Pipe<CS, 1024>,
}

impl MyLogger {
    pub fn new() -> Self {
        Self {
            buffer: Pipe::new(),
        }
    }
}

mod logger {
    use embassy_sync::pipe::Pipe;
    use log::{Metadata, Record};
    use core::fmt::Write as _;
    use crate::logger::{CS, MyLogger};

    struct Writer<'d, const N: usize>(&'d Pipe<CS, N>);

    impl log::Log for MyLogger {
        fn enabled(&self, _metadata: &Metadata) -> bool {
            true
        }

        fn log(&self, record: &Record) {
            if self.enabled(record.metadata()) {
                let _ = write!(Writer(&self.buffer), "{}\r\n", record.args());
            }
        }

        fn flush(&self) {}
    }

    impl<'d, const N: usize> core::fmt::Write for Writer<'d, N> {
        fn write_str(&mut self, s: &str) -> Result<(), core::fmt::Error> {
            let _ = self.0.try_write(s.as_bytes());
            Ok(())
        }
    }
}
