#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]

mod config;
mod joystick;
mod input_task;
mod data_comm;
mod output_task;
mod logger;

use defmt::unwrap;
use embassy_executor::{Executor, Spawner};
use embassy_rp::adc::{Adc, Config, InterruptHandler as AdcInterruptHandler};
use embassy_rp::gpio::{Output, Level, Pin};
use embassy_rp::bind_interrupts;
use {defmt_rtt as _, panic_probe as _};
use embassy_rp::usb::{Driver, InterruptHandler};
use embassy_rp::peripherals::USB;
use embassy_usb::{Builder, class::cdc_acm::State};
use embassy_executor::_export::StaticCell;
use embassy_rp::multicore::{spawn_core1, Stack};
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::channel::Channel;
use embassy_usb::class::cdc_acm::CdcAcmClass;
use embassy_usb::class::hid::{HidWriter, State as HidState};
use crate::config::{init_usb_config, usb_task, MyRequestHandler};
use crate::data_comm::{Button, Message};
use crate::logger::{MyLogger, logger_task};
use crate::input_task::{button_task, joystick_button_task, joystick_task};
use crate::output_task::{consuming_task, HidCommunicatorType};

static mut CORE1_STACK: Stack<4096> = Stack::new();
static EXECUTOR0: StaticCell<Executor> = StaticCell::new();
static EXECUTOR1: StaticCell<Executor> = StaticCell::new();

/// Channel used to communicate data through the threads
pub static CHANNEL: Channel<CriticalSectionRawMutex, Message, 1> = Channel::new();

bind_interrupts!(struct Irqs {
    USBCTRL_IRQ => InterruptHandler<USB>;
    ADC_IRQ_FIFO => AdcInterruptHandler;
});

/// Macro used to allocate memory that will be usable through
/// a reference and available throughout the whole session.
///
/// This macro comes from the Embassy examples.
macro_rules! singleton {
    ($val:expr) => {{
        type T = impl Sized;
        static STATIC_CELL: StaticCell<T> = StaticCell::new();
        let (x,) = STATIC_CELL.init(($val,));
        x
    }};
}

/// Main task and entry point of our program.
#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let l_peripherals = embassy_rp::init(Default::default());

    // == Config related ==
    let driver = Driver::new(l_peripherals.USB, Irqs);
    let (config, hid_config) = init_usb_config(singleton!(MyRequestHandler {}));
    log::info!("Retrieved the configs");

    let mut builder = Builder::new(
        driver,
        config,
        &mut singleton!([0; 256])[..],
        &mut singleton!([0; 256])[..],
        &mut singleton!([0; 256])[..],
        &mut singleton!([0; 64])[..],
    );

    // Writer used to send the event data through USB
    let hid_writer = HidWriter::<Driver<USB>, 5>::new(&mut builder, singleton!(HidState::new()), hid_config);

    // Used to log data through USB
    let acm_logger = CdcAcmClass::new(&mut builder,  singleton!(State::new()), 64);
    let usb = builder.build();
    let logger = singleton!(MyLogger::new());


    // == Initiating peripherals ==

    // --- Joystick related ---
    let adc = Adc::new(l_peripherals.ADC, Irqs, Config::default());

    // - Joystick pins -
    let l_joy_x = l_peripherals.PIN_27;
    let l_joy_y = l_peripherals.PIN_26;
    let l_joystick_button = l_peripherals.PIN_16.degrade();

    // --- Buttons pins ---
    let l_up_button = l_peripherals.PIN_6.degrade(); // red
    let l_down_button = l_peripherals.PIN_7.degrade(); // yellow
    let l_right_button = l_peripherals.PIN_8.degrade(); // blue
    let l_left_button = l_peripherals.PIN_9.degrade(); // green

    // --- Buttons power source ---
    let _buttons_power_source = Output::new(l_peripherals.PIN_18, Level::High);

    // == Tasks ==
    // --- Handling the input tasks ---
    spawn_core1(l_peripherals.CORE1, unsafe { &mut CORE1_STACK }, move || {
        let executor1 = EXECUTOR1.init(Executor::new());
        executor1.run(|spawner| {
            unwrap!(spawner.spawn(button_task(l_up_button, Button::Up)));
            unwrap!(spawner.spawn(button_task(l_left_button,Button::Left)));
            unwrap!(spawner.spawn(button_task(l_down_button,Button::Down)));
            unwrap!(spawner.spawn(button_task(l_right_button,Button::Right)));
            unwrap!(spawner.spawn(joystick_button_task(l_joystick_button)));
            unwrap!(spawner.spawn(joystick_task(adc, l_joy_x, l_joy_y)));
        });
    });

    // --- Handling the output tasks ---
    let executor0 = EXECUTOR0.init(Executor::new());
    executor0.run(|spawner| {
        unwrap!(spawner.spawn(usb_task(usb)));
        unwrap!(spawner.spawn(logger_task(acm_logger, logger, log::LevelFilter::Info)));
        unwrap!(spawner.spawn(consuming_task(HidCommunicatorType::USB(hid_writer))));
    });
}

