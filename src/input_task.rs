use embassy_rp::gpio::{AnyPin, Input, Pull};
use embassy_rp::adc::Adc;
use embassy_rp::peripherals::{PIN_26, PIN_27};
//use embassy_time::{Duration, Timer};
use crate::CHANNEL;
use crate::data_comm::{Button, ButtonState, Message};


/// Task handling the button press for each button.
///
/// It waits for the button to be pressed/released through an interrupt
/// then sends the corresponding message through the channel.
/// We have only 4 buttons with the current setup so we can't really go higher than 4.
#[embassy_executor::task(pool_size = 4)]
pub async fn button_task(p_button_pin: AnyPin, p_button: Button) {
    let mut l_button_as_input = Input::new(p_button_pin, Pull::Up);

    loop {

        l_button_as_input.wait_for_rising_edge().await;
        log::info!("Button {:?} pressed", p_button);
        CHANNEL.send(Message::get_button_data(p_button.clone(), ButtonState::Pressed)).await;
        log::info!("Button {:?} waiting for release", p_button);
        l_button_as_input.wait_for_falling_edge().await;
        log::info!("Button {:?} released", p_button);
        CHANNEL.send(Message::get_button_data(p_button.clone(), ButtonState::Released)).await;
    }
}

/// Task handling the joystick button.
/// This is a different task as the behaviour is inverted (when you compare it with the basic buttons)
#[embassy_executor::task]
pub async fn joystick_button_task(p_button_pin: AnyPin) {
    let mut l_joy_button_as_input = Input::new(p_button_pin, Pull::Up);

    loop {
        l_joy_button_as_input.wait_for_falling_edge().await;
        log::info!("Button Joy pressed");
        CHANNEL.send(Message::get_button_data(Button::Joy, ButtonState::Pressed)).await;
        log::info!("Button Joy waiting for release");
        l_joy_button_as_input.wait_for_rising_edge().await;
        log::info!("Button Joy released");
        CHANNEL.send(Message::get_button_data(Button::Joy, ButtonState::Released)).await;
    }
}

/// Task handling the joystick.
///
/// Reads the ADC pins for the joystick and sends the corresponding message through the channel.
/// We only have one joystick (and 3 ADC pins) on this setup so this task doesn't need to be more generic for now
#[embassy_executor::task]
pub async fn joystick_task(mut adc: Adc<'static>, mut pin_x: PIN_27, mut pin_y: PIN_26   /*, p_x_led: AnyPin, p_y_led: AnyPin, p_left_led: AnyPin, p_right_led: AnyPin, p_up_led: AnyPin, p_down_led: AnyPin*/) {
    loop {
//        Timer::after(Duration::from_millis(100)).await;


        let x = adc.read(&mut pin_x).await;
        let y = adc.read(&mut pin_y).await;

        let x = convert_joystick_value(x);
        let y = convert_joystick_value(y);

        // log::info!("x: {0} | x_hexa: {0:#x}, y: {1} | y_hexa: {1:#x}", x, y);

        CHANNEL.send(Message::get_joystick_data(x, y)).await;
    }
}

/// Function used to reverse the joystick's scale.
fn convert_joystick_value(v:u16) -> u16 {
    v.abs_diff(4096)
}