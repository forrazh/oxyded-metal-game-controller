use defmt::info;
use embassy_rp::peripherals::USB;
use embassy_rp::usb::Driver;
use embassy_usb::Config;
use embassy_usb::class::hid::{Config as HidConfig, ReportId, RequestHandler};
use embassy_usb::control::OutResponse;
use crate::joystick::ControllerReport;
use usbd_hid::descriptor::SerializedDescriptor;
// use crate::joystick::CONTROLLER_DESCRIPTOR;

/// Creates the static configurations for the controller.
/// Handles the `usb` and `HID` configs.
pub fn init_usb_config(p_request_handler: &'static MyRequestHandler) -> (Config<'static>, HidConfig<'static>) {
    let mut config =  Config::new(0x0000, 0xcafe);
    config.manufacturer = Some("hfrz");
    config.product = Some("Oxyded Game Controller");
    config.serial_number = Some("000000000001");
    config.max_power = 100;
    config.max_packet_size_0 = 64;

    config.device_class = 0xEF;
    // config.device_sub_class = GamePad.into();
    // config.device_protocol = 0x01;

    let  hid_config = HidConfig {
        report_descriptor: ControllerReport::desc(),
        // report_descriptor: CONTROLLER_DESCRIPTOR,
        // report_descriptor: MouseReport::desc(),
        request_handler: Some(p_request_handler),
        poll_ms: 60,
        max_packet_size: 8,
    };
    
    (config, hid_config)
}

/// Used to make the device discoverable as `USB`.
#[embassy_executor::task]
pub async fn usb_task(mut usb: embassy_usb::UsbDevice<'static, Driver<'static, USB>>) {
    usb.run().await
}

/// Request Handler for the HID.
pub struct MyRequestHandler {}
impl RequestHandler for MyRequestHandler {
    fn get_report(&self, id: ReportId, _buf: &mut [u8]) -> Option<usize> {
        info!("Get report for {:?}", id);
        None
    }

    fn set_report(&self, id: ReportId, data: &[u8]) -> OutResponse {
        info!("Set report for {:?}: {=[u8]}", id, data);
        OutResponse::Accepted
    }

    fn set_idle_ms(&self, id: Option<ReportId>, dur: u32) {
        info!("Set idle rate for {:?} to {:?}", id, dur);
    }

    fn get_idle_ms(&self, id: Option<ReportId>) -> Option<u32> {
        info!("Get idle rate for {:?}", id);
        None
    }
}